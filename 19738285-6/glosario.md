# Glosario

1. **Control de Versiones(VC):** Es un sistema que registra los cambios que se hacen sobre un archivo o conjunto de estos, de forma que se pueda recuperar distintas versiones a futuro.(git-scm.com)
2. **Control de versiones distribuido (DVC):** Permite la colaboración en grupo en un proyecto en común sin ser necesario compartir una misma red.(git-scm.com)
3. **Repositorio remoto:** Son versiones del proyecto que se encuentran depositados en Internet o en la red. Se puede tener varios,  puede ser de sólo lectura, o lectura y escritura.(git-scm.com)
4. **Repositorio local:** El repositorio local es el que se encuentra en el computador. (git-scm.com)
4. **Copia de trabajo / Working Copy:** La copia de trabajo es un respaldo de la información del repositorio o el trabajo.(git-scm.com)
5. **Área de Preparación / Staging Area:** Guarda información acerca de qué cambios en el repositorio irán en el siguiente commit a realizar.(git-scm.com)
6. **Preparar Cambios / Stage Changes:** Marca archivos modificados de forma que figuren en el área de preparación.(git-scm.com)
7. **Confirmar cambios / Commit Changes:** Las ediciones que se realizan a los archivos rastreados por git se guardan en la hebra o branch.(git-scm.com)
8. **Commit:** Es un comando que se usa para guardar los cambios en el repositorio local.(git-scm.com)
9. **Clone:** Hace una copia de un repositorio y la almacena en la maquina local.(git-scm.com)
10. **Pull:** Este comando actualiza el repositorio local con los datos remotos.(https://www.atlassian.com/git/tutorials/syncing/git-fetch)
11. **Push:** Este comando se usa para publicar nuevas actualizaciones locales en un servidor remoto.(https://www.atlassian.com/git/tutorials/syncing/git-fetch)
12. **Fetch:** Este comando Descarga el contenido remoto, pero no actualiza el estado de trabajo del repositorio local.(https://www.atlassian.com/git/tutorials/syncing/git-fetch)
13. **Merge:** Este comando es para fusionar 2 ramas.(git-scm.com)
14. **Status:** Este comando es para verificar el estado de los archivos.(git-scm.com)
15. **Log:** Con este comando se puede acceder a distinta información, en diversos formatos y con varios niveles de detalle. Se puede visualizar las líneas nuevas y borradas en un archivo de código; ver los commits de un proyecto con su id (SHA).(git-scm.com)
16. **Checkout:** Este comando permite cambiar de rama. (git-scm.com)
17. **Rama / Branch:** En un repositorio cada commit posee un puntero al commit que viene, que da como resultado así este desarrollo incremental en el cual se basa el control de versiones. Un commit padre puede apuntar a más de un commit hijo, generándose así un desarrollo en paralelo de versiones del proyecto, donde cada hijo puede ser trabajado de manera aislada. Este desarrollo en paralelo se le conoce como ramas del repositorio.(git-scm.com)
18. **Etiqueta / Tag:**  Con tag se puede agregar etiquetas en alguna parte del desarrollo del proyecto.(git-scm.com)




 











